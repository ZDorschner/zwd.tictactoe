#pragma once

#include <string>
#include <iostream>

using namespace std;


class TicTacToe
{
private: // you know what these are most likely
	char m_board[3][3] = { {'1','2','3'},{'4','5','6'},{'7','8','9'} };
	int row, col;
	char m_playerTurn = 'X';
	bool draw = false;

	public:
		
		void DisplayBoard() const // Displays the cool tictactoe board
		{
			cout << "	|	|		\n";
			cout << m_board[0][0] << "	|"<< m_board[0][1] <<"	|"<< m_board[0][2] << "\n";
			cout << "________|_______|______\n";
			cout << "	|	|		\n";
			cout << m_board[1][0] << "	|" << m_board[1][1] << "	|" << m_board[1][2] << "\n";
			cout << "________|_______|______\n";
			cout << "	|	|		\n";
			cout << m_board[2][0] << "	|" << m_board[2][1] << "	|" << m_board[2][2] << "\n";
			cout << "	|	|		\n";

		}
		// checks to see if its over
		bool IsOver()
		{
			for (int i = 0; i < 3; i++)
			{ // check side to side and up and down
				if (m_board[i][0] == m_board[i][1] && m_board[i][0] == m_board[i][2] ||
					m_board[0][i] == m_board[1][i] && m_board[0][i] == m_board[2][i])
				{
					return true;
				}
				
			}// checks diagonally
			if (m_board[0][0] == m_board[1][1] && m_board[0][0] == m_board[2][2] ||
					m_board[0][2] == m_board[1][1] && m_board[0][2] == m_board[2][0])
				{ 
					return true;
				}
			
			for (int i = 0; i < 3; i++)
			{
				for (int j = 0; j < 3; j++)
				{
					if (m_board[i][j] != 'X' && m_board[i][j] != 'O') 
					{
						return false;
					}
						
				}

			} // if over makes draw true
			draw = true;
			return true;

			
		}

		char GetPlayerTurn() const // see what turn it is
		{

			if (m_playerTurn == 'X')
			{
				m_playerTurn == 'O';
				return 'X';
			}
			else
			{
				m_playerTurn == 'X';
				return 'O';
			}
		}

		bool IsValidMove(int position) // checks the move validation
		{
			
			switch (position) {
					case 1: row = 0; col = 0; break;
					case 2: row = 0; col = 1; break;
					case 3: row = 0; col = 2; break;
					case 4: row = 1; col = 0; break;
					case 5: row = 1; col = 1; break;
					case 6: row = 1; col = 2; break;
					case 7: row = 2; col = 0; break;
					case 8: row = 2; col = 1; break;
					case 9: row = 2; col = 2; break;
					default:
						return false;
			}
			
		}

		void Move(int position)
		{
			if (m_playerTurn == 'X' && m_board[row][col] != 'X' && m_board[row][col] != 'O')
			{
				m_board[row][col] = 'X'; // set the X/O then swaps
				m_playerTurn = 'O';
			}
			else if (m_playerTurn == 'O' && m_board[row][col] != 'X' && m_board[row][col] != 'O')
			{
				m_board[row][col] = 'O'; // set the X/O then swaps
				m_playerTurn = 'X';
			}
			else
			{
				cout << "Position already in use\n"; // checks to see if spot has been used
				GetPlayerTurn();
			}
		}

		void DisplayResult() // looks to see who won
		{
			if (m_playerTurn == 'O' && draw == false)
			{
				cout << "Player One Won!\n";
			}
			else if (m_playerTurn == 'X' && draw == false)
			{
				cout << "Player Two Won!\n";
			}
			else
			{
				cout << "It's a draw\n";
			}
		}
};